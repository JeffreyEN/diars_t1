﻿using Diars_Espinoza_Nolasco_Jeffrey_T1_Examen.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Diars_Espinoza_Nolasco_Jeffrey_T1_Examen.Controllers
{
    public class PostController : Controller
    {
        private readonly T1_Diars_Context context;
        // GET: PostController
        public PostController(T1_Diars_Context context)
        {
            this.context = context;
        }
        [HttpGet]
        public ActionResult Index()
        {
            var post = context.Posts.OrderByDescending(o => o.Fecha_Post).ToList();
            return View(post);
        }

        [HttpPost]
        public ActionResult Registrar(Postcs post)
        {
            post.Fecha_Post = DateTime.Now;
            context.Posts.Add(post);
            context.SaveChanges();
            return RedirectToAction("Index");
        }
        [HttpGet]
        public ActionResult Registrar()
        {
            return View("Registrar", new Postcs());
        }
        [HttpGet]
        public ActionResult Detalle(int id)
        {
            var post = context.Posts.Where(o => o.Id == id).FirstOrDefault();
            ViewBag.Coments = context.Coments.Where(o => o.IdPost == id).OrderByDescending(o => o.Fecha_Comentario).ToList();
            return View(post);
        }
        [HttpPost]
        public ActionResult RegistrarComentario(Coments coments)
        {
            coments.Fecha_Comentario = DateTime.Now;
            context.Add(coments);
            context.SaveChanges();
            return RedirectToAction("Index");
        }
        [HttpGet]
        public ActionResult RegistrarComentario(int idP)
        {
            ViewBag.Id = idP;
            Console.WriteLine(ViewBag.Id);
            return View("RegistrarComentario",new Coments());
        }
    }
}