﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Diars_Espinoza_Nolasco_Jeffrey_T1_Examen.Models
{
    public class Coments
    {
        public int Id { get; set; }
        public String Opinion { get; set; }
        public DateTime Fecha_Comentario { get; set; }
        public int IdPost { get; set; }
    }
}
