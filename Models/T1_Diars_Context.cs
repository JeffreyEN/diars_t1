﻿using Diars_Espinoza_Nolasco_Jeffrey_T1_Examen.Models.Mapeo;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Diars_Espinoza_Nolasco_Jeffrey_T1_Examen.Models
{
    public class T1_Diars_Context : DbContext
    {
        public DbSet<Postcs> Posts { get; set; }
        public DbSet<Coments> Coments { get; set; }
        public T1_Diars_Context(DbContextOptions<T1_Diars_Context> options)
            : base(options) { }
            protected override void OnModelCreating(ModelBuilder modelBouilder)
        {
            modelBouilder.ApplyConfiguration(new Post_Mapeo());
            modelBouilder.ApplyConfiguration(new ComentsMapeo());
        }

    }
}
