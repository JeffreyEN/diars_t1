﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Diars_Espinoza_Nolasco_Jeffrey_T1_Examen.Models.Mapeo
{
    public class ComentsMapeo : IEntityTypeConfiguration<Coments>
    {

        public void Configure(Microsoft.EntityFrameworkCore.Metadata.Builders.EntityTypeBuilder<Coments> builder)
        {
            builder.ToTable("Coments");
            builder.HasKey(o => o.Id);
        }
    }
}
