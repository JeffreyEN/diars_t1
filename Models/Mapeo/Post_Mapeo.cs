﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Diars_Espinoza_Nolasco_Jeffrey_T1_Examen.Models.Mapeo
{
    public class Post_Mapeo : IEntityTypeConfiguration<Postcs>
    {
        public void Configure(Microsoft.EntityFrameworkCore.Metadata.Builders.EntityTypeBuilder<Postcs> builder)
        {
            builder.ToTable("Postcs");
            builder.HasKey(o => o.Id);

            builder.HasMany(o => o.Coments).WithOne().HasForeignKey(o => o.IdPost);
        }
    }
}
