using System;

namespace Diars_Espinoza_Nolasco_Jeffrey_T1_Examen.Models
{
    public class ErrorViewModel
    {
        public string RequestId { get; set; }

        public bool ShowRequestId => !string.IsNullOrEmpty(RequestId);
    }
}
