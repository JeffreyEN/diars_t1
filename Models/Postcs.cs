﻿using Diars_Espinoza_Nolasco_Jeffrey_T1_Examen.Models.Mapeo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Diars_Espinoza_Nolasco_Jeffrey_T1_Examen.Models
{
    public class Postcs
    {
        public int Id { get; set; }
        public String Nombre_Post { get; set; }
        public String Autor_Post { get; set; }
        public String Contenido_Post { get; set; }
        public DateTime Fecha_Post { get; set; }
        public List<Coments> Coments { get; set; }


    }
}
